import React from 'react'
import { AnimatePresence, motion } from 'framer-motion'

import FacebookIcon from '@public/icons/facebook.svg'
import GithubIcon from '@public/icons/github.svg'
import GitlabIcon from '@public/icons/gitlab.svg'
import GmailIcon from '@public/icons/gmail.svg'
import LinkedinIcon from '@public/icons/linkedin.svg'

import { useTranslation } from '@/hooks/useTranslation'
import NextLink from '@/components/NextLink'

const variants = (delay = 0.4) => {
  return {
    offscreen: {
      opacity: 0,
      y: 50
    },
    onscreen: {
      opacity: 1,
      y: 0,
      transition: {
        delay,
        duration: 1
      }
    }
  }
}

const Footer = () => {
  const t = useTranslation()

  const connectIcons = () => (
    <div className='flex items-center gap-2'>
      <NextLink href='https://www.linkedin.com/in/nguyen-chi-trung-5631761aa/' target='_blank'>
        <LinkedinIcon className='cursor-pointer' />
      </NextLink>
      <NextLink href='https://github.com/NguyenChiTrung1310' target='_blank'>
        <GithubIcon className='[&>path]:fill-white-primary cursor-pointer' />
      </NextLink>
      <NextLink href='https://gitlab.com/chitrung.nguyen' target='_blank'>
        <GitlabIcon className='cursor-pointer' />
      </NextLink>
      <NextLink href='https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=nguyenchitrung1310@gmail.com' target='_blank'>
        <GmailIcon className='cursor-pointer' />
      </NextLink>
      <NextLink href='https://www.facebook.com/chitrung.nguyen.5832' target='_blank'>
        <FacebookIcon className='cursor-pointer' />
      </NextLink>
    </div>
  )

  const desktopView = () => (
    <div className='text-white-primary'>
      <div className='grid grid-cols-4 gap-4 lg:gap-12'>
        <motion.div variants={variants()} className='flex flex-col justify-center col-span-2'>
          <h6 className='uppercase font-medium'>{t('common.footer.about')}</h6>
        </motion.div>
        <motion.div variants={variants()} className='flex flex-col justify-center col-span-1'>
          <h6 className='uppercase font-medium'>{t('common.footer.connect')}</h6>
        </motion.div>
        <motion.div variants={variants()} className='flex flex-col justify-center col-span-1'>
          <h6 className='uppercase font-medium'>{t('common.footer.contact')}</h6>
        </motion.div>
      </div>
      <div className='grid grid-cols-4 gap-4 lg:gap-12'>
        <motion.div variants={variants()} className='pt-5 col-span-2'>
          <p className='text-sm whitespace-pre-line'>{t('common.footer.bio')}</p>
        </motion.div>
        <motion.div variants={variants()} className='pt-5 col-span-1'>
          {connectIcons()}
        </motion.div>
        <motion.div variants={variants()} className='pt-5 col-span-1'>
          <p className='text-sm'>+84 963742905</p>
          <p className='pt-2 text-sm'>nguyenchitrung1310@gmail.com</p>
        </motion.div>
      </div>
      <motion.div variants={variants(1)} className='text-center mt-10'>
        <hr />
        <p className='mt-10 text-sm'>
          Copyright ©2022 All rights reserved | This portfolio is made by <span className='font-bold text-primary'>Trung Nguyen</span>
        </p>
      </motion.div>
    </div>
  )

  const mobileView = () => (
    <div className='text-white-primary'>
      <motion.div variants={variants()} className=''>
        <h6 className='uppercase'>{t('common.footer.about')}</h6>
      </motion.div>
      <motion.div variants={variants()} className='pt-5'>
        <p className='text-sm whitespace-pre-line'>{t('common.footer.bio')}</p>
      </motion.div>
      <motion.div variants={variants(0.8)} className='mt-10 flex xs_max:flex-col items-start justify-between gap-10 xs:gap-16'>
        <div className='xs:w-1/2'>
          <h6 className='uppercase'>{t('common.footer.connect')}</h6>
          <div className='pt-5'>{connectIcons()}</div>
        </div>
        <div className='xs:w-1/2'>
          <h6 className='uppercase'>{t('common.footer.contact')}</h6>
          <p className='text-sm pt-5'>+84 963742905</p>
          <p className='pt-2 text-sm'>nguyenchitrung1310@gmail.com</p>
        </div>
      </motion.div>
      <div className='text-center pt-10'>
        <motion.hr variants={variants(1)} />
        <motion.p variants={variants(1)} className='hidden xs:block mt-10 text-sm'>
          Copyright ©2022 All rights reserved | This portfolio is made by <span className='font-bold text-primary'>Trung Nguyen</span>
        </motion.p>
        <motion.p variants={variants(1)} className='block xs:hidden mt-10 text-sm'>
          Copyright ©2022 All rights reserved
        </motion.p>
        <motion.p variants={variants(1)} className='block xs:hidden text-sm'>
          This portfolio is made by <span className='font-bold text-primary'>Trung Nguyen</span>
        </motion.p>
      </div>
    </div>
  )

  return (
    <footer className='h-auto footer-gradient'>
      <AnimatePresence mode='wait'>
        <motion.div initial='offscreen' whileInView='onscreen' viewport={{ once: true, amount: 0.8 }} className='container-xl h-auto tablet:flex tablet:items-center py-10'>
          <div className='hidden tablet:block'>{desktopView()}</div>
          <div className='block tablet:hidden'>{mobileView()}</div>
        </motion.div>
      </AnimatePresence>
    </footer>
  )
}

export default Footer
