import React, { useCallback } from 'react'
import NProgress from 'nprogress'
import { usePathname } from 'next/navigation'
import { AnimatePresence, motion } from 'framer-motion'

import Footer from './footer'
import Header from './header'
import RouterEventWrapper from './RouterEvent'

const RootLayout = ({ children }: React.PropsWithChildren) => {
  const pathname = usePathname()
  const onStart = useCallback(() => {
    NProgress.set(0.4)
    return NProgress.start()
  }, [])
  const onComplete = useCallback(() => NProgress.done(), [])

  return (
    <RouterEventWrapper onStart={onStart} onComplete={onComplete}>
      <AnimatePresence mode='wait'>
        <div className='relative' id='default-layout'>
          <Header />
          <motion.main
            key={pathname}
            initial={{ y: 10, opacity: 0 }}
            animate={{ y: 0, opacity: 1 }}
            exit={{ y: -10, opacity: 0 }}
            transition={{ duration: 0.75 }}
            className='w-full h-auto container-xl mt-[72px]'
          >
            {children}
          </motion.main>
          <Footer />
        </div>
      </AnimatePresence>
    </RouterEventWrapper>
  )
}

export default RootLayout
