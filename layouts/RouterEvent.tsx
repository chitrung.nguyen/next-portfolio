import React, { useEffect, useState } from 'react'
import { usePathname, useSearchParams } from 'next/navigation'

import RouterContext from '@context/RouterContext'

interface Props {
  onStart?: () => void
  onComplete?: () => void
}

const RouterEventWrapper = ({ children, onStart = () => undefined, onComplete = () => undefined }: React.PropsWithChildren<Props>) => {
  const [isChanging, setIsChanging] = useState(false)
  const pathname = usePathname()
  const searchParams = useSearchParams()

  useEffect(() => setIsChanging(false), [pathname, searchParams])
  useEffect(() => {
    if (isChanging) onStart()
    else onComplete()
  }, [isChanging])

  return <RouterContext.Provider value={() => setIsChanging(true)}>{children}</RouterContext.Provider>
}

export default RouterEventWrapper
