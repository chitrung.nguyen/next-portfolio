import React, { memo } from 'react'
import Image from 'next/image'
import { AnimatePresence, motion } from 'framer-motion'

import { hrefUrl } from '@/utils'
import { ROUTE } from '@/utils/routes'
import Logo from '@public/buffalo.png'
import useScrollView from '@/hooks/useScrollView'

import Menu from '@components/Menu'
import NextLink from '@/components/NextLink'
import MenuMobile from '@components/MenuMobile'
import DarkmodeSwitcher from '@components/DarkmodeSwitcher'
import LanguageSwitcher from '@components/LanguageSwitcher'

const Header: React.FC = () => {
  const { scrollY, visible } = useScrollView()

  return (
    <motion.div>
      <AnimatePresence mode='wait'>
        {visible && (
          <motion.nav
            initial={{ scaleY: 0, transformOrigin: 'top', pointerEvents: 'none' }}
            animate={{ scaleY: 1, transformOrigin: 'top', pointerEvents: 'auto' }}
            exit={{ scaleY: 0, transformOrigin: 'top', pointerEvents: 'none' }}
            className={['w-full h-[72px] bg-white-shade-1 dark:layout-gradient fixed top-0 left-0 z-[1000] transition-all duration-75', scrollY > 10 && 'shadow-lg'].filter(Boolean).join(' ')}
          >
            <div className='h-full flex-between container-xl'>
              <NextLink className='text-darkGrey dark:text-white-primary flex-center' href={hrefUrl(ROUTE.HOME)}>
                <Image src={Logo} alt='Logo' width={40} height={40} />
                <h5 className='pl-2'>nct.</h5>
              </NextLink>

              {/* MENU LIST */}
              <div className='hidden tablet:block'>
                <Menu />
              </div>

              {/* ACTIONS MENU */}
              <div className='flex-center space-x-2'>
                <div className='hidden tablet:block'>
                  <LanguageSwitcher />
                </div>
                <DarkmodeSwitcher />
                <div className='block tablet:hidden'>
                  <MenuMobile />
                </div>
              </div>
            </div>
          </motion.nav>
        )}
      </AnimatePresence>
    </motion.div>
  )
}

export default memo(Header)
