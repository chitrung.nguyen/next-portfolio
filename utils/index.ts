import { currentLanguage, defaultLocale } from '@/i18n'

export const hrefUrl = (url: string) => {
  const language = currentLanguage()
  if (defaultLocale !== language) {
    return `/${language}/${url}`
  }

  return url
}

export const formatPathname = (pathname: string) => {
  const language = currentLanguage()
  return language === defaultLocale ? pathname : (pathname as string).replace(`/${language}`, '')
}
