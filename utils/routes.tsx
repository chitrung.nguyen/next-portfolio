import { HomeIcon, IdentificationIcon, CodeBracketSquareIcon, NewspaperIcon } from '@heroicons/react/24/outline'

export enum ROUTE {
  HOME = '/',
  PROJECTS = '/projects',
  ABOUT = '/about',
  BLOGS = '/blogs'
}

export type Routes = {
  link: string
  name: string
  id?: string
  icon?: any
}

const style = 'w-5 h-5 text-darkGrey'

export const routes: Routes[] = [
  {
    link: ROUTE.HOME,
    name: 'Introduction',
    id: 'introduction',
    icon: <HomeIcon className={style} />
  },
  {
    link: ROUTE.ABOUT,
    name: 'About',
    id: 'about',
    icon: <IdentificationIcon className={style} />
  },
  {
    link: ROUTE.PROJECTS,
    name: 'Projects',
    id: 'projects',
    icon: <CodeBracketSquareIcon className={style} />
  },
  {
    link: ROUTE.BLOGS,
    name: 'Blogs',
    id: 'blogs',
    icon: <NewspaperIcon className={style} />
  }
]
