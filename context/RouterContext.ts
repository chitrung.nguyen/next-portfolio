'use client'

import { createContext } from 'react'

const RouterContext = createContext<() => void>(() => undefined)

export default RouterContext
