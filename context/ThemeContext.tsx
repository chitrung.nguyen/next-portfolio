/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { createContext, useState } from 'react'

interface ITheme {
  theme: string
}

type ThemeType = ITheme | undefined | 'dark' | 'light'
type IThemeContext = [ThemeType, React.Dispatch<React.SetStateAction<ThemeType>>]

const getInitialTheme = () => {
  if (typeof window !== 'undefined' && window.localStorage) {
    const storedPrefs = window.localStorage.getItem('color-theme')
    if (typeof storedPrefs === 'string') {
      return storedPrefs
    }

    const userMedia = window.matchMedia('(prefers-color-scheme: dark)')
    if (userMedia.matches) {
      return 'dark'
    }
  }

  return 'light'
}

export const ThemeContext = createContext<IThemeContext>(['light', () => null])

export const ThemeProvider = ({ initialTheme, children }: any) => {
  const [theme, setTheme] = useState<ThemeType>(undefined)

  const rawSetTheme = (rawTheme: any) => {
    const root = window.document.documentElement
    const isDark = rawTheme === 'dark'

    root.classList.remove(isDark ? 'light' : 'dark')
    root.classList.add(rawTheme)

    localStorage.setItem('color-theme', rawTheme)
  }

  if (initialTheme) {
    rawSetTheme(initialTheme)
  }

  React.useEffect(() => {
    if (theme) {
      rawSetTheme(theme)
    } else {
      setTheme(getInitialTheme() as ThemeType)
    }
  }, [theme])

  return <ThemeContext.Provider value={[theme, setTheme]}>{children}</ThemeContext.Provider>
}
