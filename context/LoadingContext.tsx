import { usePathname } from 'next/navigation'
import React, { createContext, useEffect, useState } from 'react'

interface ILoading {
  loading: boolean
}

type LoadingType = ILoading | boolean

type LoadingContext = [LoadingType, React.Dispatch<React.SetStateAction<LoadingType>>]

export const LoadingContext = createContext<LoadingContext>([false, () => null])

export const LoadingProvider = ({ initialLoading, children }: any) => {
  const [loading, setLoading] = useState<LoadingType>(false)
  const pathname = usePathname()

  useEffect(() => {
    if (loading) {
      setLoading(false)
    }
  }, [pathname])

  return <LoadingContext.Provider value={[loading, setLoading]}>{children}</LoadingContext.Provider>
}
