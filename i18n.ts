import { usePathname } from 'next/navigation'
import resourceEN from './dictionaries/en.json'
import resourceVI from './dictionaries/vi.json'

/* eslint-disable @typescript-eslint/no-non-null-assertion */
export const defaultLocale = 'en'
export const locales = ['en', 'vi'] as const
export type ValidLocale = (typeof locales)[number]

type PathnameLocale = {
  pathname: string
  locale?: never
}
type ISOLocale = {
  pathname?: never
  locale: string
}

type LocaleSource = PathnameLocale | ISOLocale

export const currentLanguage = () => {
  const pathname = usePathname()
  if (pathname === '/vi' || pathname?.startsWith('/vi/')) {
    return 'vi'
  }

  return 'en'
}

export const getLocalePartsFrom = ({ pathname, locale }: LocaleSource) => {
  if (locale) {
    const localeParts = locale.toLowerCase().split('-')
    return {
      lang: localeParts[0]
    }
  } else {
    const pathnameParts = pathname!.toLowerCase().split('/')
    return {
      lang: pathnameParts[1]
    }
  }
}

// const dictionaries: Record<ValidLocale, any> = {
//   en: () => import('dictionaries/en.json').then(module => module.default),
//   vi: () => import('dictionaries/vi.json').then(module => module.default)
// } as const
const dictionaries: Record<ValidLocale, any> = {
  en: resourceEN,
  vi: resourceVI
} as const

export const getTranslator = (locale: ValidLocale) => {
  const dictionary = dictionaries[locale]

  return (key: string, params?: { [key: string]: string | number | JSX.Element }) => {
    let translation = key.split('.').reduce((obj, key) => obj && obj[key], dictionary)
    if (!translation) {
      return key
    }
    if (params && Object.entries(params).length) {
      Object.entries(params).forEach(([key, value]) => {
        translation = translation!.replace(`{{ ${key} }}`, String(value))
      })
    }
    return translation
  }
}
