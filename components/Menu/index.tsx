import React, { memo } from 'react'
import { usePathname } from 'next/navigation'

import { formatPathname, hrefUrl } from '@/utils'
import { Routes, routes } from '@/utils/routes'
import { useTranslation } from '@/hooks/useTranslation'

import Button from '@components/@cores/Button'
import NextLink from '@components/NextLink'

const Menu = () => {
  const pathname = usePathname()
  const t = useTranslation()

  const isActive = (url: string) => {
    return formatPathname(pathname as string) === url
  }

  return (
    <div className='w-auto h-full'>
      <ul className='flex h-full items-center gap-8'>
        {routes.map((menu: Routes) => {
          return (
            <li className='relative inline-block transition-all duration-300 menu-effects' key={menu.id}>
              <Button className='border-none p-0 text-darkGrey dark:text-white-primary font-medium text-sm'>
                <NextLink className={['py-2', isActive(menu.link) && 'text-secondary dark:text-primary'].filter(Boolean).join(' ')} title={menu.name} href={hrefUrl(menu.link)}>
                  {t(`navbar.${menu.id}`)}
                </NextLink>
              </Button>
            </li>
          )
        })}
      </ul>
    </div>
  )
}

export default memo(Menu)
