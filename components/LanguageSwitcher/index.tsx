import React, { memo } from 'react'
import { usePathname } from 'next/navigation'
import { AnimatePresence, motion } from 'framer-motion'

import { currentLanguage, locales } from '@/i18n'

import NextLink from '@components/NextLink'
import Button from '@components/@cores/Button'

interface Props {
  isMobile?: boolean
}

const LanguageSwitcher: React.FC<Props> = ({ isMobile }) => {
  const language = currentLanguage()
  const pathname = usePathname()

  const pathNameHandler = () => {
    if (pathname?.startsWith('/') && language === 'en') {
      return `/${locales.filter(locale => locale !== language).join(' ')}${pathname}`
    }

    return pathname?.replace('/vi/', '/')
  }

  return (
    <>
      {isMobile ? (
        <ul className='flex-center space-x-3'>
          {locales.map(lng => (
            <li key={lng} className={`relative ${lng === 'vi' ? '' : 'before:absolute before:top-0 before:-right-[6px] before:bg-darkGrey before:block before:w-[1px] before:h-5'}`}>
              <NextLink href={pathNameHandler() as string}>
                <p
                  title={lng}
                  className={`leading-5 tracking-[0.25px] uppercase cursor-pointer relative ${
                    language === lng ? 'text-secondary dark:text-primary font-bold underline leading-[22px] underline-offset-4' : 'font-normal text-darkGrey'
                  }`}
                >
                  {lng}
                </p>
              </NextLink>
            </li>
          ))}
        </ul>
      ) : (
        <NextLink href={pathNameHandler() as string}>
          <AnimatePresence mode='wait' initial={false}>
            <motion.div style={{ display: 'inline-block' }} key={language} initial={{ y: -20, opacity: 0 }} animate={{ y: 0, opacity: 1 }} exit={{ y: 20, opacity: 0 }} transition={{ duration: 0.1 }}>
              <Button className='border-none dark:bg-white-primary bg-darkGrey dark:text-darkGrey text-white-primary font-semibold uppercase p-0 h-10 w-10'>
                {locales.filter(locale => locale !== language).join(' ')}
              </Button>
            </motion.div>
          </AnimatePresence>
        </NextLink>
      )}
    </>
  )
}

export default memo(LanguageSwitcher)
