import React, { memo, useContext } from 'react'
import { AnimatePresence, motion } from 'framer-motion'

import { ThemeContext } from '@context/ThemeContext'
import { MoonIcon, SunIcon } from '@heroicons/react/24/solid'

import Button from '@components/@cores/Button'

const DarkmodeSwitcher: React.FC = () => {
  const [theme, setTheme] = useContext(ThemeContext)

  const toggleDarkMode = () => {
    setTheme(theme === 'dark' ? 'light' : 'dark')
  }
  return (
    <AnimatePresence mode='wait' initial={false}>
      <motion.div style={{ display: 'inline-block' }} key={theme as string} initial={{ y: -20, opacity: 0 }} animate={{ y: 0, opacity: 1 }} exit={{ y: 20, opacity: 0 }} transition={{ duration: 0.2 }}>
        <Button primary onClick={toggleDarkMode} className={['h-10 w-10 flex items-center justify-center', theme === 'light' && 'bg-secondary'].filter(Boolean).join(' ')}>
          {theme === 'dark' ? <SunIcon className='h-5 w-5 text-white-primary' /> : <MoonIcon className='h-5 w-5 text-white' />}
        </Button>
      </motion.div>
    </AnimatePresence>
  )
}

export default memo(DarkmodeSwitcher)
