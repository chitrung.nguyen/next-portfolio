import React from 'react'
import './style.scss'

const Loading = () => {
  return <span className='loading' />
}

export default Loading
