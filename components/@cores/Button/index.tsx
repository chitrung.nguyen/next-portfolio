import React, { memo, ReactNode } from 'react'
import { AnimatePresence, motion } from 'framer-motion'

import Spinner from '@components/@cores/Spinner'

interface Props {
  children: ReactNode
  className?: string
  classNameDeep?: string
  primary?: boolean
  secondary?: boolean
  disabled?: boolean
  loading?: boolean
  prefix?: ReactNode
  suffix?: ReactNode
  onClick?: () => void
  type?: 'button' | 'submit' | 'reset'
  value?: string
  loadingColor?: 'primary' | 'white' | 'black' | 'darkGrey'
}

const Button: React.FC<Props> = ({
  className,
  classNameDeep,
  children,
  primary = false,
  secondary = false,
  prefix,
  suffix,
  type = 'button',
  value = '',
  loading = false,
  loadingColor = 'white',
  ...props
}) => {
  const prefixIcon = () => (
    <>
      {prefix && !loading && (
        <motion.span initial={{ opacity: 0 }} animate={{ opacity: loading ? 0 : 1, transition: { duration: 0.3 } }} className='pr-[10px] mt-[1px]'>
          {prefix}
        </motion.span>
      )}
    </>
  )

  const sufixIcon = () => (
    <>
      {suffix && !loading && (
        <motion.span initial={{ opacity: 0, marginLeft: 16 }} animate={{ opacity: loading ? 0 : 1, marginLeft: 0, transition: { duration: 0.3 } }} className='pl-[10px] mt-[1px]'>
          {suffix}
        </motion.span>
      )}
    </>
  )
  return (
    <AnimatePresence>
      <motion.button transition={{ duration: 0.3 }} type={type} value={value} className={[className, 'btn', primary ? 'btn-primary' : null, secondary ? 'btn-secondary' : null].join(' ')} {...props}>
        <div className={[classNameDeep, 'relative flex-center'].filter(Boolean).join(' ')}>
          {prefixIcon()}
          <div>{children}</div>
          {sufixIcon()}

          <AnimatePresence>
            {loading && (
              <motion.div initial={{ marginLeft: 0 }} animate={{ marginLeft: 16, transition: { duration: 0.3 } }} exit={{ marginLeft: -16, transition: { duration: 0.3 } }}>
                <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1, transition: { duration: 0.1 } }} exit={{ opacity: 0, transition: { duration: 0.1 } }}>
                  <Spinner color={loadingColor} />
                </motion.div>
              </motion.div>
            )}
          </AnimatePresence>
        </div>
      </motion.button>
    </AnimatePresence>
  )
}

export default memo(Button)
