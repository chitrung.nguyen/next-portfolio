import React, { memo } from 'react'
import { usePathname } from 'next/navigation'

import AnchorLink from './AnchorLink'

type Props = {
  href: string
  title?: string
  className?: string
  target?: string
}

const NextLink = ({ href, children, className, title, target }: React.PropsWithChildren<Props>) => {
  const pathname = usePathname()

  return (
    <AnchorLink target={target} title={title} href={href} className={[className, pathname === href && 'font-bold'].filter(Boolean).join(' ')}>
      {children}
    </AnchorLink>
  )
}

export default memo(NextLink)
