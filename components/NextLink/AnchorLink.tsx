import React, { memo, useContext } from 'react'
import Link from 'next/link'

import RouterContext from '@context/RouterContext'

type Props = {
  href: string
  className?: string
  title?: string
  target?: string
}

const AnchorLink = ({ href, children, className, title, target }: React.PropsWithChildren<Props>) => {
  const startChange = useContext(RouterContext)
  const useLink = href && href.startsWith('/')

  if (useLink)
    return (
      <Link
        target={target}
        title={title}
        href={href}
        onClick={() => {
          const { pathname, search, hash } = window.location
          if (href !== pathname + search + hash) startChange()
        }}
        className={className}
      >
        {children}
      </Link>
    )

  return (
    <a href={href} className={className} target={target}>
      {children}
    </a>
  )
}

export default memo(AnchorLink)
