import React, { memo, useState } from 'react'
import { usePathname } from 'next/navigation'
import { motion, Variants } from 'framer-motion'

import { Routes, routes } from '@utils/routes'
import { formatPathname, hrefUrl } from '@/utils'
import { Bars3Icon } from '@heroicons/react/24/solid'
import { useTranslation } from '@/hooks/useTranslation'

import NextLink from '@components/NextLink'
import Button from '@components/@cores/Button'
import LanguageSwitcher from '@components/LanguageSwitcher'

import './style.scss'

const itemVariants: Variants = {
  open: {
    opacity: 1,
    y: 0,
    transition: { type: 'spring', stiffness: 400, damping: 20 }
  },
  closed: { opacity: 0, y: [-4, -10, 20], transition: { duration: 0.3 } }
}

const mainVariants: Variants = {
  open: {
    clipPath: 'inset(0% 0% 0% 0% round 9px)',
    transition: {
      type: 'spring',
      bounce: 0,
      duration: 0.7,
      delayChildren: 0.3,
      staggerChildren: 0.05
    }
  },
  closed: {
    clipPath: 'inset(10% 10% 90% 90% round 9px)',
    transition: {
      type: 'spring',
      bounce: 0,
      duration: 0.3,
      delay: 0.3
    }
  }
}

const MenuMobile = () => {
  const pathname = usePathname()
  const t = useTranslation()
  const [toggle, setToggle] = useState<boolean>(false)

  return (
    <motion.nav initial={false} animate={toggle ? 'open' : 'closed'} className='relative menu-mobile'>
      <Button onClick={() => setToggle(!toggle)} className='h-10 w-10 flex items-center justify-center dark:border-white-primary border-darkGrey'>
        <Bars3Icon className='h-5 w-5 dark:text-white-primary text-darkGrey' />
      </Button>

      <motion.ul
        variants={mainVariants}
        className={['fixed right-5 h-max w-52 top-20 py-2 rounded-lg bg-white border border-solid border-gray-200 z-[1000]', toggle ? 'pointer-events-auto' : 'pointer-events-none']
          .filter(Boolean)
          .join(' ')}
      >
        {routes.map((menu: Routes) => {
          return (
            <motion.li variants={itemVariants} key={menu.name}>
              <NextLink title={menu.name} href={hrefUrl(menu.link)}>
                <Button
                  classNameDeep='justify-start'
                  prefix={menu.icon}
                  onClick={() => setToggle(false)}
                  className={[
                    'btn-menu w-full border-none font-bold py-4 tracking-[1.25px] text-left',
                    formatPathname(pathname as string) === menu.link && 'bg-secondary dark:bg-primary text-white active'
                  ]
                    .filter(Boolean)
                    .join(' ')}
                >
                  {t(`navbar.${menu.id}`)}
                </Button>
              </NextLink>
            </motion.li>
          )
        })}
        <motion.hr variants={itemVariants} />

        <motion.div className='p-1 mt-2' variants={itemVariants}>
          <LanguageSwitcher isMobile />
        </motion.div>
      </motion.ul>
    </motion.nav>
  )
}

export default memo(MenuMobile)
