import { getLocalePartsFrom, getTranslator, locales, ValidLocale } from '@/i18n'
import React from 'react'

export async function generateStaticParams() {
  return locales.map(locale => getLocalePartsFrom({ locale }))
}

const AboutPage = ({ params }: any) => {
  const translate = getTranslator(
    `${params.lang}` as ValidLocale // our middleware ensures this is valid
  )
  return (
    <div>
      <p className='text-red-400'>About page</p>
      <h1>{translate('welcome.helloWorld')}</h1>
      <h2>
        {translate('welcome.happyYear', {
          year: new Date().getFullYear()
        })}
      </h2>
    </div>
  )
}

export default AboutPage
