'use client'

import { useTranslation } from '@/hooks/useTranslation'
import { getLocalePartsFrom, locales } from '@/i18n'

interface Props {
  params: { lang: string; country: string }
}

export function generateStaticParams() {
  return locales.map(locale => getLocalePartsFrom({ locale }))
}

const Home: React.FC<Props> = ({ params }) => {
  const t = useTranslation()

  return (
    <div>
      <p className='text-red-400'>Home page {t('welcome.helloWorld')}</p>
    </div>
  )
}

export default Home
