'use client'

import Layout from '@/layouts'
import { ThemeProvider as ThemeProviderContext } from '@context/ThemeContext'
import { LoadingProvider as LoadingProviderContext } from '@context/LoadingContext'
import './styles/globals.scss'

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang='en'>
      {/*
        <head /> will contain the components returned by the nearest parent
        head.tsx. Find out more at https://beta.nextjs.org/docs/api-reference/file-conventions/head
      */}
      <meta content='width=device-width, initial-scale=1' name='viewport' />
      <head />
      <body>
        <LoadingProviderContext>
          <ThemeProviderContext>
            <Layout>{children}</Layout>
          </ThemeProviderContext>
        </LoadingProviderContext>
      </body>
    </html>
  )
}
