import { useEffect, useState } from 'react'

function useScrollView(): { scrollY: number; visible: boolean } {
  const [scrollY, setScrollY] = useState<number>(0)
  const [visible, setVisible] = useState<boolean>(true)

  const handleScroll = () => {
    if (window.scrollY > scrollY && window.scrollY > 80) {
      // if scroll down hide the navbar
      setVisible(false)
    } else {
      // if scroll up show the navbar
      setVisible(true)
    }

    setScrollY(window.scrollY)
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [scrollY])

  return { scrollY, visible }
}

export default useScrollView
