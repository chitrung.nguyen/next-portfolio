'use client'
import { currentLanguage, getTranslator, ValidLocale } from '@/i18n'

export const useTranslation = () => {
  const language = currentLanguage()
  return getTranslator(`${language}` as ValidLocale)
}
